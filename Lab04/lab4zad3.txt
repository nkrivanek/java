lab4 zad3.

public abstract class Shape {
    public abstract double area();
    public abstract  double circumference();

}


 public class Circle extends Shape{
        Circle(double r){ this.r=r;}

        public double getRadius(){
            return r;
        };

       public double area(){
            return PI*r*r;
        };


       public double circumference() {
            return 2*PI*r;
        };

        public double PI=3.14;
        protected double r;

    }




    public class Rectangle extends Shape{
        Rectangle (double w,double h){
            this.w=w;
            this.h=h;
        }
        public double getWidth(){ return w;}
        public double getHeight(){return h;}
        public double area(){
            return w*h;
        };
        public double circumference() {
            return 2*w*h;
        };

        protected double w;
        protected double h;


    }

